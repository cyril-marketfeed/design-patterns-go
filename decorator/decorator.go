package main

import "fmt"

type Pizza interface {
	getPrice() int
}

type VeggieMania struct {
}

func (p *VeggieMania) getPrice() int {
	return 15
}

type TomatoTopping struct {
	pizza Pizza
}

func (c *TomatoTopping) getPrice() int {
	pizzaPrice := c.pizza.getPrice()
	return pizzaPrice + 7
}

type CheeseTopping struct {
	pizza Pizza
}

func (c *CheeseTopping) getPrice() int {
	pizzaPrice := c.pizza.getPrice()
	return pizzaPrice + 10
}

// -  Client Code

func main() {

	pizza := &VeggieMania{}

	/// Add cheese topping
	pizzaWithCheese := &CheeseTopping{
		pizza: pizza,
	}

	/// Add tomato topping
	pizzaWithCheeseAndTomato := &TomatoTopping{
		pizza: pizzaWithCheese,
	}

	fmt.Printf(
		"Price of veggieMania with tomato and cheese topping is %d\n",
		pizzaWithCheeseAndTomato.getPrice(),
	)
}
