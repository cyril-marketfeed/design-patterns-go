package main

import "fmt"

func removeFromslice(
	observerList []Observer, observerToRemove Observer,
) []Observer {
	observerListLength := len(observerList)
	for i, observer := range observerList {
		if observerToRemove.getID() == observer.getID() {
			observerList[observerListLength-1], observerList[i] =
				observerList[i], observerList[observerListLength-1]
			return observerList[:observerListLength-1]
		}
	}
	return observerList
}

type Observer interface {
	update(string)
	getID() string
}

type Subject interface {
	register(observer Observer)
	deregister(observer Observer)
	notifyAll()
}

/// Concrete Observer

type Customer struct {
	id string
}

func (c *Customer) update(itemName string) {
	fmt.Printf(
		"Sending email to customer %s for item %s\n",
		c.id, itemName,
	)
}
func (c *Customer) getID() string {
	return c.id
}

/// Concrete Subject

type Item struct {
	observerList []Observer
	name         string
	inStock      bool
}

func newItem(name string) *Item {
	return &Item{
		name: name,
	}
}

func (i *Item) register(o Observer) {
	i.observerList = append(i.observerList, o)
}
func (i *Item) deregister(o Observer) {
	i.observerList = removeFromslice(
		i.observerList, o)
}
func (i *Item) notifyAll() {
	for _, observer := range i.observerList {
		observer.update(i.name)
	}
}
func (i *Item) updateAsInStock() {
	fmt.Printf("Item %s is now in stock\n", i.name)
	i.inStock = true
	i.notifyAll()
}

// - Client Code

func main() {

	/// Concrete Subject
	shirtItem := newItem("Nike Shirt")

	observer1 := &Customer{id: "abc@gmail.com"}
	observer2 := &Customer{id: "xyz@gmail.com"}

	shirtItem.register(observer1)
	shirtItem.register(observer2)

	shirtItem.updateAsInStock()
}
