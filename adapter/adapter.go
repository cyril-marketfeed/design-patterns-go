package main

import "fmt"

type Computer interface {
	InsertIntoLightningPort()
}

type Phone struct {
}

func (c *Phone) InsertLightningConnectorIntoComputer(computer Computer) {
	fmt.Println("Client inserts Lightning connector into computer.")
	computer.InsertIntoLightningPort()
}

type Mac struct {
}

func (m *Mac) InsertIntoLightningPort() {
	fmt.Println("Lightning connector is plugged into mac machine.")
}

type Windows struct {
}

func (w *Windows) insertIntoUSBPort() {
	fmt.Println("USB connector is plugged into windows machine.")
}

type WindowsAdapter struct {
	windowsMachine *Windows
}

func (w *WindowsAdapter) InsertIntoLightningPort() {
	fmt.Println("Adapter converts Lightning signal to USB.")
	w.windowsMachine.insertIntoUSBPort()
}

// -  Client Code

func main() {

	phone := &Phone{}
	mac := &Mac{}

	phone.InsertLightningConnectorIntoComputer(
		mac)

	windowsMachine := &Windows{}
	windowsMachineAdapter := &WindowsAdapter{
		windowsMachine: windowsMachine,
	}

	phone.InsertLightningConnectorIntoComputer(
		windowsMachineAdapter)
}
