package main

import "fmt"

type User struct {
	name string
	age  int
}

type Iterator interface {
	hasNext() bool
	getNext() *User
}

type UserIterator struct {
	index int
	users []*User
}

func (u *UserIterator) hasNext() bool {
	if u.index < len(u.users) {
		return true
	}
	return false

}
func (u *UserIterator) getNext() *User {
	if u.hasNext() {
		user := u.users[u.index]
		u.index++
		return user
	}
	return nil
}

// / Program to an interface, not an implementation.
type Collection interface {
	createIterator() Iterator
}

type UserCollection struct {
	users []*User
}

func (u *UserCollection) createIterator() Iterator {
	return &UserIterator{
		users: u.users,
	}
}

// -  Client Code

func main() {

	user1 := &User{
		name: "Alice",
		age:  20,
	}
	user2 := &User{
		name: "Bob",
		age:  40,
	}
	user3 := &User{
		name: "Charlie",
		age:  60,
	}

	userCollection := &UserCollection{
		users: []*User{user1, user2, user3},
	}

	iterator := userCollection.createIterator()

	for iterator.hasNext() {
		user := iterator.getNext()
		fmt.Printf("User is %+v \n", user)
	}
}
