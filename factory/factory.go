package main

import "fmt"

type Gun interface {
	getName() string
	getPower() int
}

type BaseGun struct {
	name  string
	power int
}

func (bg *BaseGun) getName() string {
	return bg.name
}
func (bg *BaseGun) getPower() int {
	return bg.power
}

type Ak47 struct {
	BaseGun
	CurrentPopularityInRussia int
}

func newAk47() Gun {
	baseGun := BaseGun{
		name:  "AK47 gun",
		power: 4,
	}
	return &Ak47{
		BaseGun: baseGun,
	}
}

type musket struct {
	BaseGun
	IsOutdated bool
}

func newMusket() Gun {
	baseGun := BaseGun{
		name:  "Musket gun",
		power: 1,
	}
	return &musket{
		BaseGun: baseGun,
	}
}

func getGun(gunType string) (Gun, error) {
	switch gunType {
	case "ak47":
		return newAk47(), nil
	case "musket":
		return newMusket(), nil
	default:
		return nil, fmt.Errorf("Wrong gun type passed")
	}
}

// -  Client Code

func printDetails(g Gun) {
	fmt.Printf("Gun: %s \n", g.getName())
	fmt.Printf("Power: %d \n", g.getPower())
}

func main() {
	ak47, _ := getGun("ak47")
	musket, _ := getGun("musket")

	printDetails(ak47)
	printDetails(musket)
}
